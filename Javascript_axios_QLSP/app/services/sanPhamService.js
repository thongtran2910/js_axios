const BASE_URL = "https://6271e185c455a64564b8efa7.mockapi.io";

const sanPhamServ = {
  layDanhSach: function () {
    return axios({
      url: `${BASE_URL}/san-pham`,
      method: "GET",
    });
  },
};
