// console.log(axios());

const turnOnLoading = function () {
  document.getElementById("loading").style.display = "flex";
};
const turnOffLoading = function () {
  document.getElementById("loading").style.display = "none";
};

const renderDanhSachSanPhamService = function () {
  turnOnLoading();
  sanPhamServ
    .layDanhSach()
    .then(function (res) {
      turnOffLoading();
      xuatDanhSachSanPham(res.data);
    })
    .catch(function (err) {
      turnOffLoading();
    });
};
renderDanhSachSanPhamService();
const xoaSanPhamService = function (id) {
  turnOnLoading();
  axios({
    url: `${BASE_URL}/san-pham/${id}`,
    method: "DELETE",
  })
    .then((res) => {
      turnOffLoading();
      renderDanhSachSanPhamService();
    })
    .catch((err) => {
      turnOffLoading();
    });
};
const xuatDanhSachSanPham = function (dssp) {
  var contentHTML = "";

  dssp.forEach(function (item) {
    var contentTr = `<tr>
    <td>${item.id}</td>
    <td>${item.name}</td>
    <td>${item.price}</td>
    <td>${item.imgUrl}</td>
    <td>${item.type ? "Laptop" : "Phone"}</td>
    <td>
    <button class="btn btn-danger" onclick="xoaSanPhamService(${
      item.id
    })">Xoá</button>
    <button onclick="layThongTinSanPham(${
      item.id
    })" data-toggle="modal" data-target="#myModal" class="btn btn-primary">Sửa</button>
    </td>
    </tr>`;

    contentHTML += contentTr;
  });
  document.getElementById("tblDanhSachSP").innerHTML = contentHTML;
};

// .then: xử lý thành công
// .catch: xử lý thất bại

const layThongTinTuForm = function () {
  var tenSp = document.getElementById("TenSP").value;
  var giaSp = document.getElementById("GiaSP").value;
  var hinhAnhSp = document.getElementById("HinhSP").value;
  var loaiSp = document.getElementById("loaiSP").value * 1;
  // console.log({ tenSp, giaSp, hinhAnhSp, loaiSp });

  return new SanPham(tenSp, giaSp, hinhAnhSp, loaiSp == 1 ? true : false);
};

const themMoiSp = function () {
  var newSp = layThongTinTuForm();
  turnOnLoading();
  axios({
    url: `${BASE_URL}/san-pham`,
    method: "POST",
    data: newSp,
  })
    .then((res) => {
      turnOffLoading();
      $("#myModal").modal("hide");
      renderDanhSachSanPhamService();
    })
    .catch((err) => {
      turnOffLoading();
    });
};

// show dữ liệu lên form
const renderThongTinLenForm = function (data) {
  document.getElementById("TenSP").value = data.name;
  document.getElementById("GiaSP").value = data.price;
  document.getElementById("HinhSP").value = data.imgUrl;
  document.getElementById("loaiSP").value = data.type ? "1" : "0";
};

// lấy thông tin sản phẩm theo id

const layThongTinSanPham = function (id) {
  document.getElementById("btn-add").style.display = "none";
  axios({
    url: `${BASE_URL}/san-pham/${id}`,
    method: "GET",
  })
    .then((res) => {
      renderThongTinLenForm(res.data);
      document.querySelector("#id-sp span").innerHTML = res.data.id;
    })
    .catch((err) => {});

  console.log("id", id);
};

// cập nhật sản phẩm
const capNhatSp = function () {
  var updatedSanPham = layThongTinTuForm();
  var idSp = document.querySelector("#id-sp span").innerHTML * 1;
  axios({
    url: `${BASE_URL}/san-pham/${idSp}`,
    method: "PUT",
    data: updatedSanPham,
  })
    .then((res) => {
      $("#myModal").modal("hide");
      renderDanhSachSanPhamService();
    })
    .catch((err) => {
      console.log(err);
    });
};
